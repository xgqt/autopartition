#!/usr/bin/env bash

# WARNING!!!
# Be very careful!!!

# input a device name, ex.:
#	/dev/sdd
# Be sure the DEVICE is unmounted!

set -e
export PATH

((EUID == 0)) || (
    printf "This script must be run with root privileges!\n" && exit 1
)

if [ -z "$1" ]
then
    cat <<HELP
ERROR: Missing variables!
usage: parted.sh DEVICE
example: parted.sh /dev/sde
HELP
    exit 1
else
    printf "\n\nDestroying all partitions...\n\n"
    dd if=/dev/zero of="$1" bs=512 count=1 conv=notrunc

    printf "\n\nCreating partitions...\n\n"
    parted -a optimal "$1" < parted.txt

    printf "\n\nCreating filesystems...\n\n"
    mkfs.fat -F 32 "$1"2
    mkfs.ext4 "$1"3

    printf "\n\nDone!\n\n"
fi
